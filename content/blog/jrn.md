---
title: "jrn: A super simple encrypted journal using age and neovim"
date: 2023-12-23T19:52:48+01:00
tags: [ "en" ]
---
This Thursday we were talking in the group chat about mental health and one
thing that was mentioned was journaling. I've been thinking about starting a
journal for quite some time, as my memory sucks balls and I can't really
remember what I've done last week let alone last year, but I've never actually
brought myself to start doing it.

One solution that was suggested was Moodflow, which I used exactly once, because
the second time I tried journaling into it, it forgot the entire day's entry,
so I noped out of that one quite quick. I also considered a physical journal,
but I can't be bothered to lug around *another physical object* and if my
journal isn't always a single hand movement away, I wouldn't end up using it.

So I decided to set up my own thing, with the following requirements:

1. **Password-based encryption.** I want to be able to protect my diary from the
   lowest-effort prying eyes, so it doesn't have to be super high-tech
   encryption scheme and I also need to be able to unlock it quite quickly.

2. **Backed up.** I don't want to get that stuff to get lost. If I have a literal
   backup of my own soul, I want to keep it sufficiently safe.

3. **Always on hand.** If journaling isn't as easy as opening Twitter on my
   phone, I won't do it. It has to be always reachable or I'll just never open it.

So I came up with the following script:

```bash
#!/bin/bash

git --work-tree ~/.journal --git-dir ~/.journal/.git pull
file=${1:-~/.journal/$(date --rfc-3339=date).jmd}
tempfile=$(mktemp --suffix=.md)
while ! age -d -i ~/.journal/key.age -o $tempfile $file; do echo ""; done
nvim $tempfile
age -e -o $file -r age1hec6n30yer5l4euk6fwvlznghd457wxysugreqepyj3desdg5ersjjytay $tempfile
rm $tempfile
git --work-tree ~/.journal --git-dir ~/.journal/.git add -A
git --work-tree ~/.journal --git-dir ~/.journal/.git commit -m "Edited journal"
git --work-tree ~/.journal --git-dir ~/.journal/.git push
```

(You can find the latest version
[here](https://gitlab.com/Greenscreener/dotfiles/-/blob/master/.local/bin/jrn)
if I ever decide to improve upon it)

It automatically pushes and pulls from a private git repository, making each
instance of the journal a fully fledged backup. All the files are individually
encrypted using [age](https://github.com/FiloSottile/age), using a
passphrase-protected key that is commited directly to the git repo. I originally
wanted to have it only password protected, but that would allow me to
accidentally set a different password for one entry (as each day's entry is a
separate file), so the key only acts as an enforcement of a single passphrase
and in its encrypted form is not a secret.

The only thing that's slightly iffy about this is the fact, that individual
entries are temporarily stored in the filesystem, which is just something I'll
have to be careful about and shouldn't be that much of a concern, since I'm
probably only going to be using this on devices which I don't often share with
other people.

The last thing you're probably asking is, how am I going to set this up on my
phone, so it's available even at times when I can't be bothered to pick up my
laptop? Termux is actually a very viable solution. I can run full neovim in
there and it isn't a half bad editing experience even on mobile. The only thing
that annoyed me was that you can't swipe type in termux, as that is something I
am quite used to for writing longer texts in plain language. Termux luckily has
a solution for this, called [Text Input View](https://wiki.termux.com/wiki/Touch_Keyboard#Text_Input_View),
which presents you with a normal Android text box, that can do all of the smart
keyboard bells and whistles. (Btw the whole page on the wiki is worth a read,
for instance did you know you can use the Volume Down button on your phone
instead of Ctrl?)
