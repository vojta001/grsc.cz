---
title: "Za volební výsledek Pirátů nemůže (jenom) Prchal ani Ferjenčík"
date: 2021-10-11T19:28:58+02:00
tags: [ "cs" ]
aliases: [ "/pirati.html" ]
---

Tenhle text je primárně moje vlastní utřídění myšlenek a něco, na co můžu odkázat lidi, kterým se budu pokoušet svoji interpretaci volebního výsledku vysvětlit. Částečně také vycházím z debaty několika dalšími lidmi. 

Piráti toho spoustu podělali. O zpackané kampani a nevyužitém potenciálu mluví každý druhý, včetně jich samých, o velmi profesionálně vytvořené desinformační kampani proti nim také. Co naopak spoustu lidí vynechává je, že tohle by samo o sobě ani z dálky nezpůsobilo tak špatný volební výsledek. Hlavním důvodem jejich šíleného propadu je, že nikdo z nich (ani jejich voličů) nedomyslel, jak funguje kroužkovací systém a co to pro ně může znamenat. 

Aktuální systém kroužkování funguje tak, že když kandidát získá alespoň 5 %, posune se na první místo kandidátky, i když byl třeba poslední. Jakým způsobem to tedy vypeklo Piráty? 

Kandidátka koalice byla postavená přibližně 2:1 z Pirátů vůči starostům, navíc většina kandidátek má Piráty na prvních místech. Volič Pirátů proto nemá sebemenší důvod kroužkovat, neboť by kroužkoval kandidáty na nejvyšších místech – a to je přeci zbytečné, nebo ne? Naopak většina voličů STANu kroužkovat bude, protože se jim předložené složení kandidátky nelíbí. Navíc, STAN je postavený na jednotlivých osobnostech, za každým stojí práce v jeho obci, zatímco Piráti se spíše volí jako strana – opět voliči STANu budou kroužkovat podstatně více než Pirátů. 

Pojďme si představit extrémní situaci:

- 95 % voličů koalice chce Piráty 
- 5 % voličů chce STAN

Také si představme, že to, co jsem popisoval je dohnáno do extrému, tedy že žádný volič Pirátů nekroužkuje a voliči STANu kroužkují vždy a pokaždé stejné kandidáty. V takovém případě by, i když mají drtivou většinu voličů, získali pouze čtyři mandáty (v Praze a ve Středočeském kraji dostal PiSTAN 6 mandátů, tedy by na prvních čtyřech místech vykroužkovaní Starostové a na dalších dvou Piráti). Mají 95% preference ale jen 11 % mandátů. 

Jak jsem zmiňoval, jedná se o velmi extrémní situaci a rozhodně neodpovídá skutečnosti – procento preferenčních hlasů pro Starosty se pohybovalo okolo dvacítky, Vít Rakušan dostal dokonce 43 %, navíc většina voličů nebyla vyhraněná pro pouze jednu ze stran. 

Přesto je skutečná situace až překvapivě podobná, právě proto, že kvůli 5% hranici u kroužkování je v konečném důsledku jedno, jestli kroužkuje 30 % voličů, 90, nebo 6, jde hlavně o to, jestli se dostanou nad 5 % a mohou úplně změnit výsledek voleb pro danou stranu. 

Závěrem tedy je, že byť Piráti ztratili mnoho hlasů svým vlastním i cizím přičiněním, o nejvíce je připravil vedlejší účinek jejich účasti v koalici, který by nejspíš před volbami nikoho nenapadl. To je ale realita jejich situace a výsledek jejich svobodného rozhodnutí do koalice jít. 