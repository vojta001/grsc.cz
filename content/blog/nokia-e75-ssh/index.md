---
title: "Fixing a Nokia E75 and getting SSH to run on it"
date: 2022-01-18T17:04:00+01:00
tags: [ "en" ]
---

This began, as many good ideas do, in a pub. I was with [Vojta and Sijisu]({{< relref "links" >}}) and we were nostalgically reminiscing about the first phones we had. 

My story had a bit of a sad ending, but let's start from the beginning. My first phone was a Nokia E75, one of those really businessy models with a slide-out keyboard. 

{{< blogPhoto src="promo-photo.png" alt="The E75 in all its slide-out glory." >}}

It was my dad's former work phone that he gave me when he got upgraded to a Blackberry. Even back then, I loved the thing, though I had absolutely no clue of its true potential. It was around 2010, so using the internet on it in any way was off-limits for me, because my parents wouldn't want me accidentaly enabling mobile data and having to pay large sums of money for it. I still managed to get some custom themes onto it using the desktop "PC Suite" program, but that was all I had the time to do. 

A huge disadvantage of the phone, especially for a child, was that it was, in fact, *huge*. One day, it fell out of my pocket while I was running on a rocky road. The outer chassis of the phone survived without any obvious signs of damage, but the display was dead. My parents tried sending it to a repair shop, that did "fix" it, I was told we paid "a lot of money" for it, but the display stopped working again after a day or two. That was the end of my story with this beautiful device for quite a while. 

It took me a few years to realise what a really cool and powerful phone that was and it did make me a bit angry with myself. 

But let's cut to present day. As I was telling this story in the pub, Sijisu asked me "You know you probably could fix that, right?" 

I couldn't shake the thought out of my brain, so I went through our house and after some searching, I was indeed able to find the phone, still as broken as I left it ten years ago. 

{{< blogPhoto src="broken.jpg" alt="The phone, with the keyboard out (left). The backlight turns on, but that's it (right)." >}}

I found this [fairly cromulent repair manual](https://www.manualslib.com/manual/1048170/Nokia-E75.html?page=12#manual) on manualslib.com and even though I did not have the *"Nokia Standard Toolkit version 2"*, the manual called for, I opened the phone up to see what was wrong. I removed the battery, unscrewed four screws and revealed the display module. 

{{< blogPhoto src="disassembly.jpg" alt="The removal of the battery (left), the unscrewing of the screws (center), the removal of the top glass covering the display (right)"  caption="The disassembly process" >}}

I carefully slid the display out downwards towards the keypad and looked at the number at the back. I didn't have much hope, but I tried googling the number anyway and to my big surprise, there was [a shop](https://www.fixshop.cz/nahradni-dily-nokia-n-nokia-n77/nokia-n77-n82-n786210n-n796760-e755730-e52-e55-e665330-lcd-displej-4850244-oem/) in Slovakia with next day shipping that sold it, for like 8 bucks. What? NICE! I had to buy it at this point, even for the slim chance that it would actually work. 

It came two days later, wrapped in huge amounts of packaging material.

{{< blogPhoto src="package.jpg" alt="The display module, surrounded by tons of bubble wrap.">}}

I didn't leave the phone disassembled in the meantime, so I had to disassemble it again. I kinda forgot about the *carefully sliding the display module out* part though, and tried to pry it directly upwards, which meant I snapped it in half. But it was the old and broken display, so I only had half a heart attack. 

{{< blogPhoto src="new-display.jpg" alt="The disassembled phone with the new display inserted and the old and broken one in the background." >}}

The plastic bottom piece that I assume lets radio through snapped off though, so reassembly was a bit of a pain, but I succeeded in the end. 

I turned it on and HOLY S\*\*T it works.

{{< blogPhoto src="it-works.jpg" alt="The first thing I saw when I turned the phone on – a wallpaper that I put on there in 2011." >}}

Now, I already knew the thing could *do stuff*, firstly, it had WiFi, so it could connect to the internet, but it did blow my mind how much stuff you could do with it. It has an integrated office suite, that can, btw, print just fine on my modern WiFi printer, it can read emails, connect to IRC, network shares and services I haven't even heard of. The builtin browser is a bit of a pain to use, but you can still download [Opera Mini](https://www.opera.com/mobile/basic-phones), which makes the experience quite painless.

{{< blogPhoto src="uses.jpg" alt="My blog opened in Opera Mini (left), the document that I printed straight from the phone (right)." >}}

It also has a surprisingly very good camera that can make some fairly reasonable photos. 

{{< blogPhoto src="photos-from-e75.jpg" alt="Photos taken with the E75." >}}

However, there was one itch I wanted to scratch. In the very beginning, I thought how cool of an SSH client this thing would be, with its nice and tactile QWERTY keyboard. I first thought I might need to learn how to make Symbian apps and try to port OpenSSH to it, but I first tried googling if someone hasn't done this already. What? Somebody made an actual version of [PuTTY for this thing](http://s2putty.sourceforge.net)? 

It worked basically on the first try, but it was a bit tricky to get SSH keys to work. It supports only 1024-bit RSA, in the PuTTY Private Key (.ppk) format **version 2**. That took quite a while to figure out. The actual command to generate the key was: 

````
puttygen -o nokia-key.ppk -b 1024 -t rsa --ppk-param version=2
puttygen -o nokia-key.pub -L nokia-key.ppk
````

After this, I managed to connect to my computer and use the shell almost exactly as I normally would.

{{< blogPhoto src="vim.jpg" alt="Vim running on the E75 through SSH." >}}

<figure>
    <video src="nyancat.mp4" controls></video>
	<figcaption> Nyancat running in the terminal. </figcaption>
</figure>

In conclusion, I'm really happy with this project. I was very surprised about what the phone could and can do and how fairly usable it is, even for today's standards. I also managed to find 10 year old photos that would have been otherwise lost to time, which made me very happy and reuniting with my old phone filled me with a nice bit of nostalgia. 
