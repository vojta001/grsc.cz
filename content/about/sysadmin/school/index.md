---
title: "School"
weight: 100
image:
  src: "turris.jpg"
  alt: "One of our Omnias with a rainbow script."
---
Ever since I and a few of my mates started attending my grammar school many
years ago, we've always complained whenever some of the school's infrastructure
wasn't working correctly. Sooner or later, whoever was in charge replied with:
*"If you think you can do it better, do it yourself."* So, since 2018, we've
been in charge of the [school's website]({{< relref "../../making/web/gbl-cz/"
>}}) and since 2021, the school's network, Windows and Google Workspace. 

Our network runs on a MikroTik router and a few switches, an HPE server, a bunch
of Turris Omnias and a couple other devices. We wanted to have a bit of fun with
it, so there's a lot of different VLANs, a gorgeous Grafana+Prometheus+Loki
monitoring setup that sends alerts to Telegram, Wireguardium, which is an
automatic Wireguard config generator made by [Vojta](https://vkane.cz), a custom
Windows and Linux netboot solution, a custom webscanning app, all of that
configured declaratively using Nix and NixOS, a custom system for recording
miscellaneous data about all our machines in Nix, a Windows VM that manages all
printing and a lot of other cool stuff. Most of it is in Gitlab, but in private
repos, because we don't trust ourselves not to accidentally publish credentials
in there 😁

{{< blogPhoto src="grafana.png" alt="Grafana view of traffic going through devices in the network." format="png">}}

{{< blogPhoto src="turris.jpg" alt="One of our Omnias with a rainbow script." >}}

