---
title: "Personal"
weight: 200
image:
  src: "x230.jpg"
  alt: "My beloved X230 with the source code for this website open in Vim."
---

My Linux journey started sometime around 2014, when I installed Ubuntu for the first time. I started with Cinnamon, then I used Unity until it was discontinued and I switched to OpenSUSE with KDE in 2017. That worked for a few years, until one day, I saw a video about configuring i3. I spent an afternoon playing around with it, but when I relogged back into KDE, it felt sloppy and way too round, like coming out of a cave, blinded by the sun. Since then, I haven't tried coming out of my cave ever again and `Super-Shift-Q` is baked into my muscle memory deeper than breathing. 

I love ThinkPads and since switching to them, I am physically unable to use a computer without a reasonable trackpoint. My current daily drivers are a T440p for home use and X230 for school and when I need to carry it on my back (the T is proper heavy). 

{{< blogPhoto src="x230.jpg" alt="My beloved X230 with the source code for this website open in Vim.">}}

I like to put everything into Git, so I have a [dotfiles repository](https://gitlab.com/Greenscreener/dotfiles) on Gitlab with most of the config files I need to get a system up and running. A list of packages and an automatic installer is currently on my todo list. 

I also play around with a Synology NAS and my latest adventures involve Home Assistant. 