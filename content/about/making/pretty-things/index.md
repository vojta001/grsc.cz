---
title: Pretty things
weight: 350
---
I sometimes have the ability to make some pretty things. It doesn't happen often
but here are a few rare examples of when it does.

### Freeform electronics

Inspired by [Jiří Praus](https://twitter.com/jipraus), I started making glowing jewelry made from SMD LEDs and wire and I ended up making quite a few.

{{< blogPhoto src="freeform.jpeg" alt="Some of the different designs of glowing jewelry I made." >}}

### Sperklers
In my last year of high school, I attended a "student company" class where we
were supposed to setup a business and sell things or something. Our company was
supposed to sell jewelry me and two of my friends made but we ended up not
having enough time to devote to the project. At least we made some cool photos
for our [instagram](https://www.instagram.com/sperklers/).

{{< blogPhoto src="sperklers.jpg" alt="The bits of jewelry that I made. Two designs with the caffeine molecule and a planet-looking little sphere." >}}



