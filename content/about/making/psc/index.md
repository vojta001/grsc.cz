---
title: "PSC"
weight: 375
image: 
  src: "assembled.jpg"
  alt: "Photo of PSC we ended up presenting during the hackathon."
---

Also know as *"Pátek Show Controller",* *"Pátek Simple Show"* or more recently *"P8"* is a project we started with a few Pátek members during the *AT&T Hackathon* that took place in 2019 in Brno. 

It started as a dedicated device, which could be used to control lights, play sounds and project videos all at the same time, using a simple, scratch-like interface. We ended up not having enough time to implement video playback during the hackathon, but we managed to demonstrate both audio and DMX output. 

{{< blogPhoto src="screenshot-v1.png" alt="Screenshot of the first version we produced during the hackathon." format="png" >}}

{{< blogPhoto src="assembled.jpg" alt="Photo of the device we ended up presenting during the hackathon." >}}

This version had a lot to be desired though. A lot of the buttons in the UI didn't actually work, the script had to be edited in Markdown with special tags and more.

### Term paper

After the hackathon, I had to write a term paper for school and I decided to write it about PSC. While working on the paper, I ended up implementing a few new features. I added groups of buttons that could be triggered all at once, buttons that activate on a delay or with an interval and buttons that call existing button groups. 

{{< blogPhoto src="screenshot-v2.png" alt="Screenshot of the version I made while writing the term paper." format="png" >}}

This was however more of a proof-of-concept, we knew we had to rework the entire thing before it could be reasonably usable.

### P8

In the summer of 2020, we decided to dedicate a week of our time to work on the project. During this, we made a decision to turn it into a software-only project and also renamed it to just *"P8"*. 

I worked on the script editor. We switched from using Markdown to a WYSIWIG editor and implemented drag and drop mechanics to add the different modules.

<figure>
    <video controls>
        <source src="p8-recording.mp4">
    </video>
    <figcaption>Video demonstration of the editor UX.</figcaption>
</figure>

Right now, the project isn't being worked on (and there's a lot of work to be done) due to limited free time in the team, but we still believe we'll make the project work at some point.

