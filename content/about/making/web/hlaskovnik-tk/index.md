---
title: "Hláškovník"
weight: 400
image: 
  src: "hlaskovnik-tk.png"
  alt: "A screenshot of hlaskovnik.tk."
---

This is probably my longest running project. It started sometime around 2015 and I am maintaining a version of it until today. It was my first web project with a proper backend and database and while developing it, I learned so much about the web and, because my friends kept attacking it incessantly, I learned about things like SQL injection, spam protection and other important ways of keeping a website (more or less) safe. A few years back I rewrote the entire thing and this new version is on [GitHub](https://github.com/Greenscreener/hlaskovnik2.0). The old version is private, because it stinks, but I might end up publishing it at some point. 

{{< blogPhoto src="hlaskovnik-tk.png" alt="A screenshot of hlaskovnik.tk" format="png">}}

