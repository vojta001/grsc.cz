---
title: "CTF"
weight: 100
---
I like to play CTFs from time to time. CTF (or Capture The Flag) is a common
type of contest in cybersecurity, where the contestants have to find a hidden
string of text (called "The Flag") and then submit it as a proof they solved the
challenge. The flag is usually obtained for instance by gaining access to a vulnerable
server or decrypting some data.

You can see most of my scores on my [CTFTime profile](https://ctftime.org/user/88906),
but, at least at the time of writing, I don't play very often. When I do, it's
usually with the [Czech Cyber Team]({{< relref "CzechCyberTeam" >}}).

