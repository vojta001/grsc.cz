---
title: "Pátek"
weight: 100
image:
  src: "banner.jpg"
  alt: "Pátek's banner on our school's fence"
---

I designed the graphical identity and logo of [Pátek]({{< relref "../../making/patek" >}}). I got mainly inspired by the logo of CESNET, where the dots represent the ASCII encoding of the first letter. I decided to improve upon this and the circles at the end of our logo are a binary representation of all the letters XORed together. I also designed our stickers, business cards and banner. 

{{< blogPhoto src="banner.jpg" alt="Pátek's banner on our school's fence" >}}

