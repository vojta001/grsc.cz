---
title: "My own"
weight: 200
image: 
  src: "logo-evolution.png"
  alt: "Evolution of my logo through the years."
---

### Logo

I've tried maintaining a graphical identity for myself for a few years now. I started by making my own logo, which was different at first, but I quickly settled for the same design I use now for basically all my profile pictures.

{{< blogPhoto src="logo-evolution.png" alt="Evolution of the logo through the years." format="png" >}}

### v1 website

Later, I made the first version of my website which was a single-page design written using the Bulma framework, inspired by [Sijisu](https://sijisu.eu)'s website at the time. It was however quite heavy, looked a bit obnoxious, missed a lot of information and last but not least I wanted to incorporate a blog.

{{< blogPhoto src="old-site.png" alt="Old version of this site." format="png" >}}



