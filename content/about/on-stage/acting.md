---
title: "Acting"
weight: 400
---

Before COVID, I did acting for one year with an amateur English theater company from Brno (don't ask how I got to Brno, long story 😁). After I joined, we toured around the country with one play, but then the whole viral mess happened and even though we fully prepared another play, we sadly didn't get a single performance out. 

I can't say I'd have a natural talent for acting or that it would be something I could see myself doing in the future, but our director could probably teach acting to a rock and standing on the stage with great people at my side is one of the most amazing experiences I've ever had. 

