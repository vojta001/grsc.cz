---
title: "Guitar"
weight: 200
---

I've played the guitar since I was little and I still attend music school today. A few years ago, I switched to an electric guitar, but I still pick the classical up once in a while. I haven't had the time to start doing anything interesting with it though, just small concerts organized by the music school. On the other hand, making horrible loud noises around a campfire with friends is one of the most fun things to do. 

